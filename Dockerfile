# FROM node:14-alpine
# RUN mkdir service-auth
# RUN echo $(ls -1)
# COPY . /service-auth
# WORKDIR service-auth
# RUN echo $(ls)
# EXPOSE 49555
# ENTRYPOINT  ./service-auth/server.js
# FROM node:14
FROM node:14-alpine
RUN mkdir -p /usr/src/app

# Create app directory
WORKDIR /usr/src/async

COPY package*.json ./

RUN npm install

COPY . .

EXPOSE 3001
CMD [ "npm", "start" ]
