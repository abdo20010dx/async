var express = require('express');
var router = express.Router();
const usersBll = require('../bll/users')

/* GET users listing. */
router.get('/', function (req, res, next) {
  res.send('respond with a resource');
});

router.post('/asyncwaterfall', (req, res, next) => {
  usersBll.getUSerData(req, res)
  next()
})
router.post('/asyncseries/items', (req, res, next) => {
  usersBll.getuseritems(req, res)
  next()
})
router.post('/asyncparallel/items', (req, res, next) => {
  usersBll.getuseritemsparallel(req, res)
  next()
})

router.post('/asyncmap/items', (req, res, next) => {
  usersBll.getuseritemsmap(req, res)
  next()
})
router.post('/asyncforeach/items', (req, res, next) => {
  usersBll.getuseritemsForEachOf(req, res)
  next()
})

module.exports = router;
