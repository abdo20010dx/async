const assert = require('chai').assert
const expect = require('chai').expect
const should = require('chai').should()
const bllusers = require('../bll/users')
var sinon = require("sinon")
var sandbox = require('sinon');


var req = {
    body: {
        id: Number,
        password: String
    }
}
var res = {
    json(obj) {
        return obj
    }
}

describe('#check user validation', function () {

    beforeEach(function () {
        var req = {
            body: {
                id: Number,
                password: String
            }
        }
        var res = {
            json(obj) {
                return obj
            }
        }


        sandbox = sinon.createSandbox();
    });

    afterEach(function () {
        sandbox.restore();
    });



    it('should not return user ', function (done) {
        req.body["id"] = 50
        req.body["password"] = 55555
        let err = bllusers.unittest(req, res)
        expect(err.status).to.equal(401)
        expect(err.message).to.equal("can't find this user")
        expect(err.error).to.equal(true)

        done()

    })


    it('should return invalid password ', function (done) {
        req.body["id"] = 1
        req.body["password"] = 5555
        let err = bllusers.unittest(req, res)
        err.status.should.to.equal(401)
        err.message.should.to.equal("invalid password")
        err.error.should.to.equal(true)

        done()

    })

    it('should return user ', function (done) {
        req.body["id"] = 1
        req.body["password"] = 55555
        let data = bllusers.unittest(req, res)
        assert.typeOf(data.password, 'number'); // without optional message
        assert.typeOf(data.username, 'string', 'data.username is a string'); // with optional message
        assert.equal(data.email, 'ahmed@gmail.com', 'data.email equal `ahmed@gmail.com`');
        assert.lengthOf(data.city, 5, 'data.city`s value has a length of 5');



        done()

    })






    it('should return items ', function (done) {
        req.body["id"] = 1
        req.body["userid"] = 1
        assert.equal(bllusers.getuseritemsmap(req, res), '4')

        done()

    })



})


