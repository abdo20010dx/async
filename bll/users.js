var async = require("async");
//var waterfall = require('async-waterfall');
const { users, posts, usersProducts } = require('../usersdata/usersdata.json')

function unittest(req, res) {

  let data = users.filter(usr => usr.id == req.body.id)

  if (data.length < 1) {

    return res.json({
      error: true,
      status: 401,
      message: "can't find this user"
    })

  } else if (data[0].password != req.body.password) {

    return res.json({
      error: true,
      status: 401,
      message: "invalid password"
    })
  }
  return res.json(data[0])
}



function getUSerData(req, res) {

  async.waterfall([
    function (callback) {
      console.log('1')
      //return users
      callback(null, users);
    },
    function (level2, callback) {
      console.log('2')

      // level1 now equals array of users data 
      let userdata = level2.filter(user => user.id == req.body.id)
      if (userdata.length < 1) return callback('cant find this user')

      callback(null, userdata[0]);

    },
    function (level3, callback) {
      console.log('3')

      // level1 now equals array of users data 
      if (level3.password != req.body.password) {
        return callback('invalid password')
      }
      callback(null, level3)
    },
    function (level4, callback) {
      console.log('4')

      // level2 now equals data of  one user
      level4['greeting'] = `welcome back ${level4.username}`
      callback(null, level4)

    }
  ], function (err, result) {
    // result now equals 'done'
    if (err) {
      console.log('errstage')
      res.json(err)
    }
    console.log('resultstage')

    res.json(result)
  }
  );
}





function getuseritems(req, res) {
  async.series([
    function (callback) {
      // return user posts ...
      callback(null, posts.filter(post => post.userid == req.body.id));
    },
    function (callback) {
      // return user products  ...
      callback(null, usersProducts.filter(product => product.userid == req.body.id));
    }
  ],
    // optional callback
    function (err, results) {
      // results is now equal to ['one', 'two']
      if (err) {
        res.json(err)
      }
      res.json(results)
    }
  );
}

function getuseritemsparallel(req, res) {
  async.series([
    function (callback) {
      // return user posts ...
      callback(null, posts.filter(post => post.userid == req.body.id));
    },
    function (callback) {
      // return user products  ...
      callback(null, usersProducts.filter(product => product.userid == req.body.id));
    }
  ],
    // optional callback
    function (err, results) {
      // results is now equal to ['one', 'two']
      if (err) {
        res.json(err)
      }
      res.json(results)
    }
  );
}


function getuseritemsmap(req, res) {
  async.map(posts,
    function (value, callback) {
      if (value.userid == req.body.id) {
        return callback(null, value);
      } else {
        callback(null)
      }


      // return user posts ...
    },
    // optional callback
    function (err, results) {
      // results is now equal to ['one', 'two']
      if (err) {
        res.json(err)
      }
      res.json(results)
    }
  );
}

const myobj = {}
function getuseritemsForEachOf(req, res) {
  async.forEachOf(usersProducts,
    function (value, index, callback) {

      myobj[index] = value
      callback()

      // return user posts ...
    },
    // optional callback
    function (err) {
      // results is now equal to ['one', 'two']
      if (err) {
        res.json(err)
      }
    }
  );
  res.json(myobj)
}






module.exports = { getUSerData, getuseritems, getuseritemsparallel, getuseritemsmap, getuseritemsForEachOf, unittest }